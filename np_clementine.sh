#!/bin/bash
METADATA=`/usr/bin/qdbus org.mpris.MediaPlayer2.clementine /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Metadata`
FOOTER="#NowPlaying"
HEADER="♪"
MAXLENGTH=140

TITLE=`echo "$METADATA" | grep ":title:" | sed -e 's/^.*:title: //g'`
ARTIST=`echo "$METADATA" | grep ":artist:" | sed -e 's/^.*:artist: //g'`
ALBUMARTIST=`echo "$METADATA" | grep ":albumArtist:" | sed -e 's/^.*:albumArtist: //g'`
ALBUM=`echo "$METADATA" | grep ":album:" | sed -e 's/^.*:album: //g'`
YEAR=`echo "$METADATA" | grep "^year:" | sed -e 's/^year: //g'`

case $1 in
  "-aa" |  "--albumartist" )
    MESSAGE="${HEADER}$TITLE / $ARTIST - $ALBUM($ALBUMARTIST, $YEAR) $FOOTER"
    if [ ${#MESSAGE} -gt $MAXLENGTH ]; then
      MESSAGE="${HEADER}$TITLE / $ARTIST - $ALBUM($ALBUMARTIST) $FOOTER"
    fi
    ;;
  * )
    MESSAGE="${HEADER}$TITLE / $ARTIST - $ALBUM($YEAR) $FOOTER"
    ;;
esac

if [ ${#MESSAGE} -gt $MAXLENGTH ]; then
  MESSAGE="${HEADER}$TITLE / $ARTIST - $ALBUM $FOOTER"
  if [ ${#MESSAGE} -gt $MAXLENGTH ]; then
    MESSAGE="${HEADER}$TITLE / $ARTIST $FOOTER"
  fi
fi

echo $MESSAGE
