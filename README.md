# nowplaying

オーディオプレーヤーで再生中の曲のメタデータを取得、整形するスクリプト。

Twitter CUI Client [`tw`](https://rubygems.org/gems/tw/)にパイプで渡してTwitter投稿するために作成したもの。
140文字に収まるように適当に属性を削ります。

## スクリプト
- `np_amarok.sh`: Amarok 2用
- `np_clementine.sh`: Clementine 用

### オプション(共通)
- `-aa`, `--albumartist`: 使用する属性にアルバムアーティストを含みます

## 動作確認環境
- `np_amarok.sh`: Kubuntu 18.04 + Amarok 2.9.0
- `np_clementine.sh`: Kubuntu 20.04 + Clementine 1.4 rc1

## カスタマイズ
スクリプト中の変数`HEADER`、`FOOTER`でヘッダー/フッターとして付与する文字列をカスタマイズできます。
